require 'spec_helper'

describe "Matomy" do

  subject { page }

  describe "get report" do

    before do
      Capybara.current_driver = :selenium
      visit 'http://console.matomycontentnetwork.com/'
      fill_in "Username", with: "jpost"
      fill_in "Password", with: "matomy123"
      click_button "Sign In"
      find("option[value='custom']").click
      fill_in "report-custom-start-date", with: "09/01/2015"
      fill_in "report-custom-end-date", with: "10/04/2015"
      check "grouping-option-2"
      check "grouping-option-3"
      check "grouping-option-4"
      click_button "Run report"
      click_link('Delivery options')
      click_link "Send as email"
      fill_in "report-send-now-email-addresses", with: "fvdsfffsd@gmail.com"
      click_button "Send Now"
      sleep(5)
    end

    # it { should have_title('Matomy Content Network - Publisher Reports') }
    it { should have_content('Report') }
    # it { should have_content('Israel') }
    # it { should have_content('banner') }
  end

end